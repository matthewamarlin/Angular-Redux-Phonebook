# Angular Redux Phonebook
## Overview
This is a sample Phonebook Application, created to illustrate the use of [Redux] within [Angular].

This project is using Angular @ 4.x.x and was generated using the [Angular CLI] @ 1.4.4

## Software Development Life Cycle (SDLC)
This project uses the [GitFlow] workflow and supports continuous integration.

## Still to be done

- Add Unit Tests
- Add Build and Deployment (CI) Pipelines to gitlab.
- Make the search control fixed in place or put in into the navbar.

## Prerequisites
- [NodeJS] - The version used as of publishing is [node-v8.11.4-x64]
  - In a command window enter `node -v` to check your installed version
- [Angular CLI] - The Angular CLI is used to host, build and generate components for the Angular projects
  - In a command shell enter `npm install -g @angular/cli`
- [Compodoc] - Component documentation tool for Angular CLI 
  - In a command shell enter `npm install -g @compodoc/compodoc`
- [VS Code] - A light weight code editor with intellisense and debugging capabilities
- [Debugger for Chrome]
  - Open VS Code, press **Ctrl+P** then enter `ext install debugger-for-chrome`

## Recommended Tools / Plugins for VS Code

Launch VS Code 'Quick Open' (Ctrl+P), enter the following commands and press enter.
- [Markdown PDF]
  - `ext install markdown-pdf`
- [Auto-Open Markdown Preview]
  - `ext install vscode-auto-open-markdown-preview`

## How to get started

The website currently runs without relying on any backend services to keep the demo simple. All you need to do to get up and running is to start the web pack development server which is built into the [Angular CLI].

### Quick Start Scripts

Scripts have been included to assist with **building, debugging and deploying** the application.
- `scripts\Debug.ps1` - Starts the website project in debug local configuration (Accessible via `localhost` only)
- `scripts\Debug.External.ps1` - Starts the project in external debug configuration (Note - you will have to edit this script to work with your IP and Hostname).
- `scripts\Build.ps1` - Builds the project (in Production mode) using the [Angular CLI]
- `scripts\Clean.ps1` - Removes all generated artefacts produced by the build.
- `scripts\GenerateDocument.ps1` - Generates documentation for the application using [Compodoc]

### Running the Website (Manually)

1. Open a command prompt in the projects root directory.
2. Run the following commands:
   - `npm install`
   - `ng serve`
3. Navigate to <http://localhost:4200> in your prefered browser.

Note: Once you run the `ng serve` command any changes you make to the website will be automatically recompiled on the fly.

### Running Tests

#### Unit Tests
Run `ng test` to execute the *unit tests* via [Karma].

#### End-to-end Tests
Run `ng e2e` to execute the *end-to-end tests* via [Protractor].
Before running the tests make sure you are serving the app via `ng serve`.

## 3rd Party Libraries Used

Listed below are the notable 3rd party libraries used, above and beyond the normal Angular 4 framework

- [Bootstrap 3] - The most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.
- [FontAwesome] - A free icon css framework
- [RxJS] - An API for asynchronous programming with observable streams
- [@angular-redux/store] - Angular Bindings for Redux
- [sweetalert2] - Customizable popup boxes with zero dependencies.


## Useful Commands

#### NodeJS / NPM
- `node -v` - Gets the version of NodeJS
- `npm -v` - Gets the version of the Node Package Manager
- `npm install` - Installs packages based on a packages.json file

#### AngularCLI
All of the following commands are part of the [Angular CLI]
- `ng serve` - hosts the project (The AngularCLI uses the [WebPack] development server internally)
- `ng build` - builds the Angular project
- `ng test` - run unit tests
- `ng e2e` - runs end to end tests

#### Compodoc
- `compodoc -p tsconfig.json --theme postmark --hideGenerator` - Generates documentation for the Angular project using **compodoc** with the *postmark* theme

## Authors
- Matthew Marlin 

[FontAwesome]: http://fontawesome.io/
[Bootstrap 3]: https://getbootstrap.com/docs/3.3/getting-started/
[NgBootstrap Demo]: https://ng-bootstrap.github.io
[RxJS]: https://github.com/ReactiveX/rxjs
[sweetalert2]: https://limonte.github.io/sweetalert2/
[@angular-redux/store]: https://github.com/angular-redux/store
[Redux]: https://github.com/reactjs/redux
[Angular CLI]: https://cli.angular.io
[NodeJS]: https://nodejs.org/en
[VS Code]: https://code.visualstudio.com
[node-v8.1.4-x64]: https://nodejs.org/dist/v8.1.4/node-v8.1.4-x64.msi
[NPM]: https://www.npmjs.com/
[Angular]: https://angular.io
[LESS]: http://lesscss.org/
[TypeScript]: https://www.typescriptlang.org
[NPM Task Runner]: https://marketplace.visualstudio.com/items?itemName=MadsKristensen.NPMTaskRunner
[Open in VS Code]: https://marketplace.visualstudio.com/items?itemName=MadsKristensen.OpeninVisualStudioCode
[Compodoc]: https://www.npmjs.com/package/@compodoc/compodoc
[Markdown PDF]: https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf
[Auto-Open Markdown Preview]: https://marketplace.visualstudio.com/items?itemName=hnw.vscode-auto-open-markdown-preview
[Debugger for Chrome]: https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome
[GitFlow]: https://datasift.github.io/gitflow/IntroducingGitFlow.html
[Protractor]: http://www.protractortest.org/
[Karma]: https://karma-runner.github.io
