import { AppPage } from './app.po';

describe('angular-redux-phonebook App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getNavbarBrandText()).toEqual('Contacts');
  });
});
