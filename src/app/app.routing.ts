import { Routes } from '@angular/router';
import { ContactsListComponent } from './core/components/contact-list/contact-list.component'
import { AddContactComponent } from './core/components/add-contact/add-contact.component'

export const appRoutes: Routes = [
  { path: '', redirectTo: '/contacts', pathMatch: 'full' },
  { path: 'contacts', component: ContactsListComponent },
  { path: 'add', component: AddContactComponent },
  { path: 'edit', redirectTo: '/contacts', pathMatch: 'full' },
  { path: 'edit/:id', component: AddContactComponent },
  { path: '**', redirectTo: 'error/404' }
];
