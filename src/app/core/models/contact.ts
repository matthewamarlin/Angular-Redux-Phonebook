export interface IContact {
  id: string,
  name: string,
  avatarUrl?: string;
  note?: string;
  organizations?: IOrganization[],
  emailAddresses?: IEmailAddress[],
  addresses?: IAddress[]
  twitter?: string;
  skype?: string,
  facebook?: string,
  linkedIn?: string,
  phoneNumbers?: IPhoneNumber[]
}

export interface IOrganization {
  name: string,
  position: string
}

export interface IAddress {
  line1: string,
  line2?: string,
  city?: string,
  state?: string,
  postcode?: string,
  country?: string
}

export interface IPhoneNumber {
  number: string,
  type: PhoneNumberType
}

export interface IEmailAddress {
  email: string,
  type: EmailAddressType
}

export enum PhoneNumberType {
  Mobile = 0,
  Home,
  Work,
  Main,
  WorkFax,
  HomeFax,
  Pager,
  Other
}

export enum EmailAddressType {
  Home,
  Work,
  Main,
  Other
}
