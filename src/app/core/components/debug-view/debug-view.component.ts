import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store'
import { IAppState } from '../../redux/store'
import { IContact } from '../../models/contact'

@Component({
  selector: 'app-debug-view',
  templateUrl: './debug-view.component.html',
  styleUrls: ['./debug-view.component.less']
})
export class DebugViewComponent implements OnInit {


  @select() contacts: IContact[];
  @select() displayContacts: IContact[];
  @select() expandedContactsIds: string[];
  @select() sampleDataLoaded: boolean;
  @select() searchQuery: string;
  @select() lastUpdate: Date;
  @select() debugMode: boolean;

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() { }
}
