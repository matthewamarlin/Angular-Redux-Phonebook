import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store'
import { IAppState } from '../../redux/store'
import { SEARCH_CONTACTS, CLEAR_SEARCH } from '../../redux/actions'

@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.less']
})
export class ContactSearchComponent implements OnInit {

  @select() contacts;
  @select() displayContacts;
  searchQuery: string;

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
    this.ngRedux.select(state => state.searchQuery).subscribe(searchQuery => {
      this.searchQuery = searchQuery;
    })
  }

  updateSearch() {
    this.ngRedux.dispatch({ type: SEARCH_CONTACTS, searchQuery: this.searchQuery });
  }

  clearSearch() {
    this.ngRedux.dispatch({ type: CLEAR_SEARCH })
    this.searchQuery = "";
  }

}
