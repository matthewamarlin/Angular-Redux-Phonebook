import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store'
import { IAppState } from '../../redux/store'
import { TOGGLE_DEBUG_WINDOW, REMOVE_SAMPLE_DATA, RELOAD_SAMPLE_DATA } from '../../redux/actions'
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  @select() debugMode;
  @select() sampleDataLoaded;
  navbarCollapsed: boolean = true;

  constructor(private ngRedux: NgRedux<IAppState>, ngbDropDownconfig: NgbDropdownConfig) {
    ngbDropDownconfig.placement = "bottom-right"
  }

  ngOnInit() { }

  toggleDebugMode() {
    this.ngRedux.dispatch({ type: TOGGLE_DEBUG_WINDOW })
  }

  removeSampleData() {
    this.ngRedux.dispatch({ type: REMOVE_SAMPLE_DATA })
  }

  reloadSampleData() {
    this.ngRedux.dispatch({ type: RELOAD_SAMPLE_DATA })
  }
}
