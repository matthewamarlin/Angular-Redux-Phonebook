import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { NgRedux, select } from '@angular-redux/store'
import { IAppState } from '../../redux/store'
import { REMOVE_CONTACT, EDIT_CONTACT, EXPAND_CONTACT, COLLAPSE_CONTACT } from '../../redux/actions'
import { IContact, IAddress, PhoneNumberType, EmailAddressType } from '../../models/contact';
import swal from 'sweetalert2';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.less']
})
export class ContactsListComponent implements OnInit {

  // Store Variables
  @select() contacts: IContact[];
  @select() displayContacts: IContact[];
  @select() expandedContactsIds: string[];

  // Make the enum visible to the template
  PhoneNumberType = PhoneNumberType;
  EmailAddressType = EmailAddressType;

  constructor(private ngRedux: NgRedux<IAppState>, private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit() { }

  remove(contact: IContact) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(() => {
      this.ngRedux.dispatch({ type: REMOVE_CONTACT, contact: contact })
      swal(
        'Deleted!',
        'Contact Successfully Deleted.',
        'success'
      )
    }).catch(swal.noop);

  }

  edit(id: number) {
    //this.ngRedux.dispatch({ type: EDIT_CONTACT, id: id })
    this.router.navigateByUrl(`/edit/${id}`);
  }

  expand(id: string) {
    this.ngRedux.dispatch({ type: EXPAND_CONTACT, id: id })
  }

  collapse(id: string) {
    this.ngRedux.dispatch({ type: COLLAPSE_CONTACT, id: id })
  }

  sanitizeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getFullAddress(address: IAddress): string {
    let result = address.line1;

    if (address.line2)
      result += ', ' + address.line2;
    if (address.city)
      result += ', ' + address.city
    if (address.postcode)
      result += ', ' + address.postcode
    if (address.city)
      result += ', ' + address.city
    if (address.state)
      result += ', ' + address.state
    if (address.country)
      result += ', ' + address.country;

    return result;
  }
}
