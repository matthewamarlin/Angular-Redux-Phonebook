import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { NgRedux, select } from '@angular-redux/store'
import { IAppState } from '../../redux/store'
import { ADD_CONTACT, EDIT_CONTACT } from '../../redux/actions'
import { IContact, IAddress, IEmailAddress, IPhoneNumber, IOrganization, PhoneNumberType, EmailAddressType } from '../../models/contact'
import swal from 'sweetalert2'
import { Observable, Subscription } from 'rxjs'
import { UrlValidators } from '../../validators/url.validator'

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.less']
})
export class AddContactComponent implements OnInit, OnDestroy {

  public form: FormGroup;

  PhoneNumberType = PhoneNumberType;
  phoneNumberTypeKeys: string[];

  EmailAddressType = EmailAddressType;
  emailAddressTypeKeys: string[];

  @select() contacts;
  @select() debugMode;

  readonly defaultAvatarUrl: string = "/assets/avatars/no-profile.png";
  avatarUrl: string = this.defaultAvatarUrl;
  avatarResolving: boolean = false;

  editMode: boolean = false;
  editId: any;

  constructor(private ngRedux: NgRedux<IAppState>, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  private routeSubscription: Subscription;
  private stateSubscription: Subscription;

  ngOnInit() {

    this.phoneNumberTypeKeys = Object.keys(this.PhoneNumberType).filter(k => !isNaN(Number(k)));
    this.emailAddressTypeKeys = Object.keys(this.EmailAddressType).filter(k => !isNaN(Number(k)));

    if (this.router.url.indexOf('edit') > -1) {

      this.editMode = true;

      this.routeSubscription = this.route.paramMap.subscribe(params => {
        this.stateSubscription = this.ngRedux.select(state => state.contacts)
          .map(contacts => {
            return contacts.filter(contact => contact.id == params.get('id'));
          })
          .subscribe(contacts => {

            if (contacts.length > 1) {
              swal("Duplicate ID Detected", "Error", 'error')
            }

            if (contacts.length == 0) {
              swal("Contact Not Found", "Error", "error")
            }

            let contact = contacts[0];
            this.initForm(contact);
            this.editId = contact.id;
          });
      });
    } else {
      this.initForm();
    }

    this.form.controls['avatarUrl'].valueChanges.subscribe(value => {
      this.avatarResolving = true;
      UrlValidators.validateImageUrl(value).throttleTime(10000).catch((error) => {
        this.avatarUrl = this.defaultAvatarUrl;
        return Observable.throw(error);
      }).finally(() => {
        this.avatarResolving = false;
      }).subscribe(() => {
        this.avatarUrl = value;
      });
    });
  }

  ngOnDestroy(): void {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe()
    }
    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
    }
  }

  initForm(contact?: IContact) {

    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      avatarUrl: [this.defaultAvatarUrl, null, UrlValidators.imageUrlValidatorAsync],
      organizations: this.formBuilder.array([]),
      emailAddresses: this.formBuilder.array([]),
      addresses: this.formBuilder.array([]),
      //skype: ['', null],
      //facebook: ['', null],
      //twitter: ['', null],
      //linkedIn: ['', null],
      phoneNumbers: this.formBuilder.array([])
    });

    //Populate form with given Contact Details

    if (contact) {
      //Repopulate Single Form Fields
      this.form.controls['name'].setValue(contact.name)
      this.form.controls['avatarUrl'].setValue(contact.avatarUrl);
      this.avatarUrl = contact.avatarUrl;

      //Repopulate FormArrays
      if (contact.addresses) {
        contact.addresses.forEach(address => this.addAddress(address));
      }

      if (contact.emailAddresses) {
        contact.emailAddresses.forEach(emailAddress => this.addEmailAddress(emailAddress));
      }

      if (contact.phoneNumbers) {
        contact.phoneNumbers.forEach(phoneNumber => this.addPhoneNumber(phoneNumber));
      }

      if (contact.organizations) {
        contact.organizations.forEach(organization => this.addOrganization(organization));
      }

      if (contact.note) {
        this.addNote(contact.note);
      }

      if (contact.skype) {
        this.addSkype(contact.skype);
      }

      if (contact.facebook) {
        this.addFacebook(contact.facebook);
      }

      if (contact.linkedIn) {
        this.addLinkedIn(contact.linkedIn);
      }

      if(contact.twitter){
        this.addTwitter(contact.twitter);
      }
    }
  }

  initEmailAddress(emailAddress?: IEmailAddress) {
    return this.formBuilder.group({
      email: [emailAddress ? emailAddress.email : '', Validators.email],
      type: [emailAddress ? emailAddress.type : 0, null]
    });
  }

  initOrganization(organization?: IOrganization) {
    return this.formBuilder.group({
      name: [organization ? organization.name : '', Validators.required],
      position: [organization ? organization.position : '', null]
    });
  }

  initAddress(address?: IAddress) {
    return this.formBuilder.group({
      line1: [address ? address.line1 : '', Validators.required],
      line2: [address ? address.line2 : '', null],
      city: [address ? address.city : '', null],
      state: [address ? address.state : '', null],
      postcode: [address ? address.postcode : '', null],
      country: [address ? address.country : '', null],
    });
  }

  initPhoneNumber(phoneNumber?: IPhoneNumber) {
    return this.formBuilder.group({
      number: [phoneNumber ? phoneNumber.number : '', Validators.required],
      type: [phoneNumber ? phoneNumber.type : 0, null]
    });
  }

  addAddress(address?: IAddress) {
    const control = <FormArray>this.form.get('addresses');
    control.push(this.initAddress(address));
  }

  addEmailAddress(emailAddress?: IEmailAddress) {
    const control = <FormArray>this.form.get('emailAddresses');
    control.push(this.initEmailAddress(emailAddress));
  }

  addOrganization(organization?: IOrganization) {
    const control = <FormArray>this.form.get('organizations');
    control.push(this.initOrganization(organization));
  }

  addPhoneNumber(phoneNumber?: IPhoneNumber) {
    const control = <FormArray>this.form.get('phoneNumbers');
    control.push(this.initPhoneNumber(phoneNumber));
  }

  addNote(note?: string) {
    this.form.addControl('note', this.formBuilder.control(note ? note : '', [Validators.required]));
  }

  addSkype(skype?: string) {
    this.form.addControl('skype', this.formBuilder.control(skype ? skype : '', [Validators.required]));
  }

  addFacebook(facebook?: string) {
    this.form.addControl('facebook', this.formBuilder.control(facebook ? facebook : '', [Validators.required]));
  }

  addLinkedIn(linkedIn?: string) {
    this.form.addControl('linkedIn', this.formBuilder.control(linkedIn ? linkedIn : '', [Validators.required]));
  }

  addTwitter(twitter?: string) {
    this.form.addControl('twitter', this.formBuilder.control(twitter ? twitter : '', [Validators.required]));
  }

  removeAddress(index: number) {
    const control = <FormArray>this.form.controls['addresses'];
    control.removeAt(index);
  }

  removeEmailAddress(index: number) {
    const control = <FormArray>this.form.controls['emailAddresses'];
    control.removeAt(index);
  }

  removeOrganization(index: number) {
    const control = <FormArray>this.form.controls['organizations'];
    control.removeAt(index);
  }

  removePhoneNumber(index: number) {
    const control = <FormArray>this.form.controls['phoneNumbers'];
    control.removeAt(index);
  }

  removeNote() {
    this.form.removeControl('note');
  }

  removeSkype() {
    this.form.removeControl('skype');
  }

  removeFacebook() {
    this.form.removeControl('facebook');
  }

  removeLinkedIn() {
    this.form.removeControl('linkedIn');
  }

  removeTwitter() {
    this.form.removeControl('twitter');
  }

  getFormArray(name: string): FormArray{
    return <FormArray> this.form.get(name)
  }

  save(contact) {

    if (this.form.valid) {

      if (!this.editMode) {

        this.ngRedux.dispatch({ type: ADD_CONTACT, contact: contact })

        swal("Contact Saved", "Saved", "success").then(() => {
          this.router.navigateByUrl("/contacts")
        })
      }
      else {

        contact.id = this.editId;

        this.ngRedux.dispatch({ type: EDIT_CONTACT, contact: contact })

        swal("Contact Saved", "Saved", "success").then(() => {
          this.router.navigateByUrl("/contacts")
        })
      }

    } else {
      this.setAsTouched(this.form);
    }
  }

  cancel() {
    swal({
      title: 'Are you sure?',
      text: "Any changes to this form will be lost!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(() => {

    }).catch(swal.noop);
  }

  // html = `<div>

  // </div>`;

  // selectAvatar() {
  //     swal({
  //         title: '<i>HTML</i> <u>example</u>',
  //         type: 'info',
  //         html: this.html,
  //         showCloseButton: true,
  //         showCancelButton: true,
  //         focusConfirm: false,
  //         // confirmButtonText:
  //         //   '<i class="fa fa-thumbs-up"></i> Great!',
  //         // confirmButtonAriaLabel: 'Thumbs up, great!',
  //         // cancelButtonText:
  //         // '<i class="fa fa-thumbs-down"></i>',
  //         // cancelButtonAriaLabel: 'Thumbs down',
  //     })
  // }

  setAsTouched(group: FormGroup | FormArray) {
    group.markAsTouched()
    for (let i in group.controls) {
      if (group.controls[i] instanceof FormControl) {
        group.controls[i].markAsTouched();
      } else {
        this.setAsTouched(group.controls[i]);
      }
    }
  }

  log(val) { console.log(val); }
}
