import { AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable'

export class UrlValidators {
  static imageUrlValidatorAsync(c: AbstractControl): Observable<{ [key: string]: any }> {
    return UrlValidators.validateImageUrl(c.value).map(success => {
      return null;
    }).catch(() => {
      return Observable.of({ validateImageUrlAsyncInvalid: true })
    });
  }

  // Unfortunately this validation method causes chrome to produce error messages in the console,
  // as it tries to find the image.
  static validateImageUrl(imageUrl: string, timeout: number = null): Observable<any> {
    return Observable.fromPromise(new Promise(function (resolve, reject) {
      var timeout = timeout || 2000;
      var timer, img = new Image();
      img.onerror = img.onabort = function () {
        clearTimeout(timer);
        reject(false);
      };
      img.onload = function () {
        clearTimeout(timer);
        resolve(true);
      };
      timer = setTimeout(function () {
        // reset .src to invalid URL so it stops previous
        // loading, but doesn't trigger new load
        img.src = "//!!!!/invalid.jpg";
        reject(false);
      }, timeout);
      img.src = imageUrl;
    }));
  }
}
