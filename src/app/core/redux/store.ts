import { SAMPLE_DATA } from '../../sample-data'
import { Guid } from '../../shared/index'
import { IContact, PhoneNumberType } from '../models/contact'
import {
  ADD_CONTACT,
  REMOVE_ALL_CONTACTS,
  REMOVE_CONTACT,
  TOGGLE_DEBUG_WINDOW,
  REMOVE_SAMPLE_DATA,
  RELOAD_SAMPLE_DATA,
  EDIT_CONTACT,
  CLEAR_SEARCH,
  SEARCH_CONTACTS,
  EXPAND_CONTACT,
  COLLAPSE_CONTACT,
} from './actions'

export interface IAppState {
  contacts: IContact[],
  displayContacts: IContact[],
  expandedContactsIds: string[]
  searchQuery: string,
  lastUpdate: Date
  debugMode: boolean
  sampleDataLoaded: boolean
}

export const INITIAL_STATE: IAppState = {
  contacts: SAMPLE_DATA,
  displayContacts: SAMPLE_DATA,
  expandedContactsIds: [],
  sampleDataLoaded: true,
  searchQuery: null,
  lastUpdate: null,
  debugMode: false
}

export function rootReducer(state: IAppState, action: any) {

  switch (action.type) {
    case ADD_CONTACT: {

      action.contact.id = generateId(state.contacts, action.contact);

      let contacts = state.contacts.concat(Object.assign({}, action.contact));
      let displayContacts = searchAndSort(contacts, state.searchQuery);

      // Return new state
      return Object.assign({}, state, {
        contacts: contacts,
        displayContacts: displayContacts,
        lastUpdate: new Date()
      });
    }
    case EDIT_CONTACT: {

      let contacts = state.contacts.filter(c => c.id != action.contact.id).concat(action.contact)
      let displayContacts = searchAndSort(contacts, state.searchQuery)

      return Object.assign({}, state, {
        contacts: contacts,
        displayContacts: displayContacts,
        lastUpdate: new Date()
      })
    }
    case REMOVE_CONTACT: {

      let contacts = state.contacts.filter(c => c.id !== action.contact.id);
      let displayContacts = searchAndSort(contacts, state.searchQuery);

      return Object.assign({}, state, {
        contacts: contacts,
        displayContacts: displayContacts,
        lastUpdate: new Date()
      });
    }
    case REMOVE_ALL_CONTACTS: {
      return Object.assign({}, state, {
        contacts: [],
        displayContacts: [],
        lastUpdate: new Date()
      });
    }
    case TOGGLE_DEBUG_WINDOW: {
      return Object.assign({}, state, {
        debugMode: !state.debugMode,
        lastUpdate: new Date()
      });
    }
    case REMOVE_SAMPLE_DATA: {

      if (state.sampleDataLoaded) {

        let contacts = state.displayContacts.filter(c => SAMPLE_DATA.find(x => x.id == c.id) == null);

        return Object.assign({}, state, {
          contacts: contacts,
          displayContacts: searchAndSort(contacts, state.searchQuery),
          lastUpdate: new Date(),
          sampleDataLoaded: !state.sampleDataLoaded
        });
      }
      else {
        return state;
      }
    }
    case RELOAD_SAMPLE_DATA: {

      if (!state.sampleDataLoaded) {

        let contacts = state.contacts.concat(SAMPLE_DATA);

        return Object.assign({}, state, {
          contacts: contacts,
          displayContacts: searchAndSort(contacts, state.searchQuery),
          lastUpdate: new Date(),
          sampleDataLoaded: !state.sampleDataLoaded
        });
      }
      else {
        return state;
      }
    }
    case SEARCH_CONTACTS: {
      return Object.assign({}, state, {
        lastUpdate: new Date(),
        searchQuery: action.searchQuery,
        displayContacts: searchAndSort(state.contacts, action.searchQuery)
      });
    }
    case CLEAR_SEARCH: {
      return Object.assign({}, state, {
        lastUpdate: new Date(),
        displayContacts: sortByContactName(state.contacts),
        searchQuery: null
      });
    }
    case EXPAND_CONTACT: {
      return Object.assign({}, state, {
        lastUpdate: new Date(),
        expandedContactsIds: state.expandedContactsIds.concat(action.id)
      });
    }
    case COLLAPSE_CONTACT: {
      return Object.assign({}, state, {
        lastUpdate: new Date(),
        expandedContactsIds: state.expandedContactsIds.filter(id => id != action.id)
      });
    }
    default: {
      return state;
    }
  }
}

export function searchAndSort(contacts: any[], searchQuery: string) {
  if (searchQuery) {
    return sortByContactName(contacts.filter(c =>
      searchObjectValues(c, searchQuery.toLocaleLowerCase(), ["id", "avatarUrl"]))
    );
  }

  return sortByContactName(contacts);
}

export function sortByContactName(contact) {
  return contact.sort((a, b) => {
    if (a.name.toLocaleLowerCase() < b.name.toLocaleLowerCase()) return -1;
    if (a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase()) return 1;
    return 0;
  });
}

export function searchObjectValues(obj, term, excludes: string[] = []): boolean {
  for (var key in obj) {
    if (!(excludes.indexOf(key) > -1)) {
      if (obj.hasOwnProperty(key)) {
        var value = obj[key];

        if (value instanceof Object) {
          if (searchObjectValues(value, term, excludes)) {
            return true;
          }
        }

        if (value.toString().toLocaleLowerCase().indexOf(term) > -1) {
          return true;
        }
      }
    }
  }

  return false;
}

export function generateId(contacts: IContact[], newContact: IContact) {

  let id = Guid.New();

  if (contacts.filter(c => c.id == id).length > 0) {
    return generateId(contacts, newContact);
  }

  return id;
}
