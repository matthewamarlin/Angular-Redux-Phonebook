import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';

// Redux
import { NgRedux, NgReduxModule } from '@angular-redux/store'
import { IAppState, rootReducer, INITIAL_STATE } from './core/redux/store'

// Bootstrap for Angular 4
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//TODO Move into core module
import { ContactsListComponent } from './core/components/contact-list/contact-list.component';
import { NavbarComponent } from './core/components/navbar/navbar.component';
import { DebugViewComponent } from './core/components/debug-view/debug-view.component';
import { AddContactComponent } from './core/components/add-contact/add-contact.component';

// Modules
import { SharedModule } from './shared/shared.module';

// Top-level app component constructs.
import { appRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { ContactSearchComponent } from './core/components/contact-search/contact-search.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactsListComponent,
    NavbarComponent,
    DebugViewComponent,
    AddContactComponent,
    ContactSearchComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgReduxModule,
    NgbModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
