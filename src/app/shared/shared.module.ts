import { NgModule } from '@angular/core';
import { Guid } from './guid';

@NgModule({
  providers: [Guid]
})
export class SharedModule {}
