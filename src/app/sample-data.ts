import { IContact, PhoneNumberType, EmailAddressType } from './core/models/contact'

export const SAMPLE_DATA: IContact[] = [
  {
    id: "6b39e362-291e-4e55-9612-f64ea2558248",
    avatarUrl: "https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAANMAAAAJGI4OWE3YTZhLWE1OTMtNGFiMy05ODMwLTE3OTQ3ZTU2NTZmMQ.jpg",
    name: "Amy Burgess",
    phoneNumbers: [
      {
        number: "086 554 3211",
        type: 0
      },
      {
        number: "021 554 5668",
        type: 1
      }
    ]
  },
  {
    id: "753a1132-afa6-45d0-9b2c-4e10d3857603",
    avatarUrl: "https://pre00.deviantart.net/8667/th/pre/i/2016/240/1/2/anonymous_profile_picture_by__emicraftz_by_emicraftz-dafnk9g.jpg",
    name: "Black Beard",
    emailAddresses: [
      {
        email: "blackbeard@6b39e362.com",
        type: 0
      }
    ]
  },
  {
    id: "2dee9986-44ac-401c-8369-876285deac2f",
    avatarUrl: "/assets/avatars/User3.png",
    name: "John",
    emailAddresses: [
      {
        email: "john@example.com",
        type: 0
      }
    ],
    phoneNumbers: [
      {
        number: "082 824 1598",
        type: 0
      }
    ]
  },
  {
    name: "Loren Tichaona",
    avatarUrl: "/assets/avatars/User1.png",
    note: "",
    organizations: [],
    emailAddresses: [],
    addresses: [],
    skype: "",
    facebook: "",
    twitter: "",
    linkedIn: "",
    phoneNumbers: [],
    id: "efb70a3c-b07e-436d-8d7a-059fb17a4318"
  },
  {
    id: "36198622-4f23-4a63-b411-f908ea53b465",
    avatarUrl: "http://cdn.hitthedeals.com/media/catalog/product/cache/2/thumbnail/600x/17f82f742ffe127f42dca9de82fb58b1/4/-/4-hour-deep-sea-fishing-trip_2.jpg",
    name: "Malcom Dane",
    emailAddresses: [
      {
        email: "brett@gmail.com",
        type: 0
      }
    ],
    phoneNumbers: [
      {
        number: "0630630631",
        type: 0
      }
    ]
  },
  {
    id: "087feccf-29e1-4054-9361-5ceafc6d59dc",
    avatarUrl: "https://avatars1.githubusercontent.com/u/1702880?v=4&s=400",
    name: "Matthew Marlin",
    note: "Friend of Amy",
    linkedIn: "matthewamarlin",
    skype: "matthewamarlin",
    twitter: "",
    addresses: [
      {
        line1: "16th Main Street",
        line2: "",
        city: "Johannesburg",
        postcode: "7441",
        state: "Gauteng",
        country: "South Africa"
      }
    ],
    organizations: [
      {
        name: "Absolute Systems",
        position: "Senior Software Engineer"
      }
    ],
    emailAddresses: [
      {
        email: "matthewamarlin@gmail.com",
        type: 0
      },
      {
        email: "matthew.marlin@absolutesys.com",
        type: 1
      }
    ],
    facebook: "matthewamarlin",
    phoneNumbers: [
      {
        number: "0636960000",
        type: 0
      },
      {
        number: "0215540000",
        type: 5
      },
      {
        number: "5563242101",
        type: 4
      }
    ]
  },
  {
    id: "196b2445-490a-4e5c-8bf4-9c67b99638ee",
    avatarUrl: "/assets/avatars/User2.png",
    name: "Nick Haywood",
    emailAddresses: [
      {
        email: "nick@example.com",
        type: 0
      }
    ],
    phoneNumbers: [
      {
        number: "0630630631",
        type: 0
      },
      {
        number: "0724591800",
        type: 7
      }
    ]
  },
  {
    id: "4ca96b22-426f-419f-b588-9158f59dea8a",
    avatarUrl: "/assets/avatars/User5.png",
    name: "Tanya Chile",
    emailAddresses: [
      {
        email: "tanya@yahoo.com",
        type: 0
      }
    ],
    phoneNumbers: [
      {
        number: "0630630631",
        type: 0
      }
    ]
  }
];
